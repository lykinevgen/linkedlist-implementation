import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;


public class LinkedListTests {
    LinkedList<Integer> linkedList;

    @Before
    public void before(){
        linkedList = new LinkedList<Integer>();
    }

    @Test
    public void addLastAndGetLast(){
        linkedList.addLast(3);
        linkedList.addLast(2);
        linkedList.addLast(1);
        Assert.assertEquals( Integer.valueOf(1), linkedList.getLast());
        Assert.assertEquals(3, linkedList.size());
    }

    @Test
    public void addFirstAndGetFirst(){
        linkedList.addFirst(3);
        linkedList.addFirst(2);
        linkedList.addFirst(1);
        Assert.assertEquals( Integer.valueOf(1), linkedList.getFirst());
        Assert.assertEquals(3, linkedList.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void getLastFromEmptyList(){
        linkedList.getLast();
    }

    @Test(expected = NoSuchElementException.class)
    public void getFirstFromEmptyList(){
        linkedList.getFirst();
    }

    @Test
    public void getByIndex(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        Assert.assertEquals(Integer.valueOf(1), linkedList.get(0));
        Assert.assertEquals(Integer.valueOf(2), linkedList.get(1));
        Assert.assertEquals(Integer.valueOf(3), linkedList.get(2));
        Assert.assertEquals(Integer.valueOf(4), linkedList.get(3));
        Assert.assertEquals(4, linkedList.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getByIndexOutOfBounds(){
        linkedList.get(2);
    }

    @Test
    public void addByIndexAndGetByIndex(){
        linkedList.add(0, 1);
        linkedList.add(1, 2);
        linkedList.add(2, 3);
        linkedList.add(3, 4);
        Assert.assertEquals(Integer.valueOf(1), linkedList.get(0));
        Assert.assertEquals(Integer.valueOf(2), linkedList.get(1));
        Assert.assertEquals(Integer.valueOf(3), linkedList.get(2));
        Assert.assertEquals(Integer.valueOf(4), linkedList.get(3));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void addByIndexOutOfBounds(){
        linkedList.add(1,2);
    }

    @Test
    public void remove(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        Assert.assertEquals(Integer.valueOf(1), linkedList.remove());
    }

    @Test
    public void size(){

    }

    @Test(expected = NoSuchElementException.class)
    public void removeFromEmptyList(){
        linkedList.remove();
    }

    @Test
    public void removeFirst(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        Assert.assertEquals(Integer.valueOf(1), linkedList.removeFirst());
        Assert.assertEquals(Integer.valueOf(2), linkedList.getFirst());
        Assert.assertEquals(2, linkedList.size());

    }

    @Test(expected = NoSuchElementException.class)
    public void removeFirstFromEmptyList(){
        linkedList.removeFirst();
    }

    @Test
    public void removeLast(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        Assert.assertEquals(Integer.valueOf(3), linkedList.removeLast());
        Assert.assertEquals(Integer.valueOf(2), linkedList.getLast());
        Assert.assertEquals(2, linkedList.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void removeLastFromEmptyList(){
        linkedList.removeFirst();
    }

    @Test
    public void removeFirstOccurrence(){
        linkedList.add(2);
        linkedList.add(1);
        linkedList.add(2);
        Assert.assertTrue(linkedList.removeFirstOccurrence(2));
        Assert.assertEquals(Integer.valueOf(1), linkedList.getFirst());
        Assert.assertEquals(Integer.valueOf(2), linkedList.getLast() );
        Assert.assertEquals(2, linkedList.size());
    }

    @Test
    public void removeLastOccurrence(){
        linkedList.add(2);
        linkedList.add(1);
        linkedList.add(2);
        Assert.assertTrue(linkedList.removeLastOccurrence(2));
        Assert.assertEquals(Integer.valueOf(1), linkedList.getLast());
        Assert.assertEquals(Integer.valueOf(2), linkedList.getFirst());
        Assert.assertEquals(2, linkedList.size());
    }

    @Test
    public void removeNonExistentFirstOccurrence(){
        linkedList.add(2);
        linkedList.add(1);
        linkedList.add(2);
        Assert.assertFalse(linkedList.removeFirstOccurrence(3));
        Assert.assertEquals(3, linkedList.size());
    }

    @Test
    public void removeNonExistentLastOccurrence(){
        linkedList.add(2);
        linkedList.add(1);
        linkedList.add(2);
        Assert.assertFalse(linkedList.removeLastOccurrence(3));
        Assert.assertEquals(3, linkedList.size());
    }

    @Test
    public void removeFirstOccurrenceFromEmptyList(){
        Assert.assertFalse(linkedList.removeFirstOccurrence(3));
        Assert.assertEquals(0, linkedList.size());
    }

    @Test
    public void removeLastOccurrenceFromEmptyList(){
        Assert.assertFalse(linkedList.removeLastOccurrence(3));
        Assert.assertEquals(0, linkedList.size());
    }

    @Test
    public void poll(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);

        Assert.assertEquals(Integer.valueOf(1),linkedList.poll());
        Assert.assertEquals(Integer.valueOf(2), linkedList.get(0));
        Assert.assertEquals(Integer.valueOf(3), linkedList.get(1));
        Assert.assertEquals(Integer.valueOf(4), linkedList.get(2));
        Assert.assertEquals(3, linkedList.size());
    }

    @Test
    public void pollFromEmptyList(){
        Assert.assertEquals(null,linkedList.poll());
    }

    @Test
    public void pollFirst(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);

        Assert.assertEquals(Integer.valueOf(1),linkedList.pollFirst());
        Assert.assertEquals(Integer.valueOf(2), linkedList.get(0));
        Assert.assertEquals(Integer.valueOf(3), linkedList.get(1));
        Assert.assertEquals(Integer.valueOf(4), linkedList.get(2));
        Assert.assertEquals(3, linkedList.size());

    }

    @Test
    public void pollFirstFromEmptyList(){
        Assert.assertEquals(null,linkedList.pollFirst());
    }

    @Test
    public void pollLast(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);

        Assert.assertEquals(Integer.valueOf(4),linkedList.pollLast());
        Assert.assertEquals(Integer.valueOf(1), linkedList.get(0));
        Assert.assertEquals(Integer.valueOf(2), linkedList.get(1));
        Assert.assertEquals(Integer.valueOf(3), linkedList.get(2));
        Assert.assertEquals(3, linkedList.size());
    }

    @Test
    public void pollLastFromEmptyList(){
        Assert.assertEquals(null,linkedList.pollLast());
    }

    @Test
    public void element(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        Assert.assertEquals(Integer.valueOf(1), linkedList.element());
        Assert.assertEquals(3, linkedList.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void elementFromEmptyList(){
        Assert.assertEquals(Integer.valueOf(1), linkedList.element());
    }

    @Test
    public void peek(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        Assert.assertEquals(Integer.valueOf(1), linkedList.peek());
        Assert.assertEquals(3, linkedList.size());
    }

    @Test
    public void peekFromEmptyList(){
        Assert.assertEquals(null, linkedList.peek());
    }

    @Test
    public void peekFirst(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        Assert.assertEquals(Integer.valueOf(1), linkedList.peek());
        Assert.assertEquals(3, linkedList.size());
    }

    @Test
    public void peekFirstFromEmptyList(){
        Assert.assertEquals(null, linkedList.peekFirst());
    }

    @Test
    public void peekLast(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        Assert.assertEquals(Integer.valueOf(3), linkedList.peekLast());
        Assert.assertEquals(3, linkedList.size());
    }

    @Test
    public void peekLastFromEmptyList(){
        Assert.assertEquals(null, linkedList.peekLast());
    }

    @Test
    public void offer(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        Assert.assertTrue(linkedList.offer(4));
        Assert.assertEquals(Integer.valueOf(1),linkedList.get(0));
        Assert.assertEquals(Integer.valueOf(2),linkedList.get(1));
        Assert.assertEquals(Integer.valueOf(3),linkedList.get(2));
        Assert.assertEquals(Integer.valueOf(4),linkedList.get(3));
    }

    @Test
    public void offerFirst(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        Assert.assertTrue(linkedList.offerFirst(4));
        Assert.assertEquals(Integer.valueOf(4),linkedList.get(0));
        Assert.assertEquals(Integer.valueOf(1),linkedList.get(1));
        Assert.assertEquals(Integer.valueOf(2),linkedList.get(2));
        Assert.assertEquals(Integer.valueOf(3),linkedList.get(3));
    }

    @Test
    public void offerLast(){
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        Assert.assertTrue(linkedList.offerLast(4));
        Assert.assertEquals(Integer.valueOf(1),linkedList.get(0));
        Assert.assertEquals(Integer.valueOf(2),linkedList.get(1));
        Assert.assertEquals(Integer.valueOf(3),linkedList.get(2));
        Assert.assertEquals(Integer.valueOf(4),linkedList.get(3));
    }

    @Test
    public void push(){
        linkedList.push(1);
        linkedList.push(2);
        linkedList.push(3);
        Assert.assertEquals(Integer.valueOf(3),linkedList.get(0));
        Assert.assertEquals(Integer.valueOf(2),linkedList.get(1));
        Assert.assertEquals(Integer.valueOf(1),linkedList.get(2));
    }

    @Test
    public void pop(){
        linkedList.push(1);
        linkedList.push(2);
        linkedList.push(3);
        linkedList.pop();
        Assert.assertEquals(Integer.valueOf(2),linkedList.get(0));
        Assert.assertEquals(Integer.valueOf(1),linkedList.get(1));
    }
}
