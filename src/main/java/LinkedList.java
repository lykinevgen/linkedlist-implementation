import java.util.*;

public class LinkedList<T> extends AbstractSequentialList<T> implements List<T>, Deque<T> {

    private Node<T> first;
    private Node<T> last;
    private int size = 0;

    class Node<T>{

        T elem;
        Node<T> next;
        Node<T> prev;

        public Node(T elem, Node<T> prev, Node<T> next) {
            this.elem = elem;
            this.next = next;
            this.prev = prev;
        }
    }

    @Override
    public boolean add(T t) {
        addLast(t);
        return true;
    }

    @Override
    public void add(int index, T element) {

        if(index == size) addLast(element);
        else{
            validateIndex(index);
            Node<T> oldNode = getNode(index);
            Node<T> newNode = new Node<>(element, oldNode.prev, oldNode );
            oldNode.prev.next = newNode;
            oldNode.next.prev = newNode;
            size++;
        }
    }

    @Override
    public T get(int index) {
        validateIndex(index);
        return getNode(index).elem;
    }

    private Node<T> getNode(int index) {
        Node<T> current;
        if ((size / 2) > index){
            current = first;
            for (int i = 0; i < index; i++) current = current.next;
        }else{
            current = last;
            for (int i = size - 1; i > index; i--) current = current.prev;
        }
        return current;
    }

    private void validateIndex(int index){
        if( index >= size || index < 0 ) throw new IndexOutOfBoundsException();
    }

    @Override
    public void addFirst(T t) {
        Node<T> newNode = new Node(t, null, first);
        if(first == null) last = newNode;
        else first.prev = newNode;
        first = newNode;
        size++;
    }



    @Override
    public void addLast(T t) {
        Node<T> newNode = new Node(t, last, null);
        if(last == null) first = newNode;
        else last.next = newNode;
        last = newNode;
        size++;
    }

    @Override
    public boolean offerFirst(T t) {
        addFirst(t);
        return true;
    }

    @Override
    public boolean offerLast(T t) {
        addLast(t);
        return true;
    }

    @Override
    public T removeFirst() {
        if(first == null) throw new NoSuchElementException();

        T elem = first.elem;
        Node<T> next = first.next;
        first.elem = null;
        first.next = null;
        first = next;
        if(next == null) last = null;
        else next.prev = null;
        size--;

        return elem;
    }

    @Override
    public T removeLast() {
        if(last == null) throw new NoSuchElementException();

        T elem = last.elem;
        Node<T> prev = last.prev;
        last.elem = null;
        last.prev = null;
        last = prev;
        if(prev == null) first = null;
        else prev.next = null;
        size--;

        return elem;
    }

    @Override
    public T pollFirst() {
        return first == null ? null : removeFirst();
    }

    @Override
    public T pollLast() {
        return last == null ? null : removeLast();
    }

    @Override
    public T getFirst() {
        if( first == null ) throw new NoSuchElementException();
        return first.elem;
    }

    @Override
    public T getLast() {
        if(last == null ) throw new NoSuchElementException();
        return last.elem;
    }

    @Override
    public T peekFirst() {
        return first == null ? null : getFirst();
 }

    @Override
    public T peekLast() {
        return last == null ? null : getLast();
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        Node<T> current = first;
        for ( ;current != null; current = current.next )
        if (current.elem.equals(o)) {
            unbind(current);
            return true;
        }

        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        Node<T> current = last;
        for ( ;current != null; current = current.prev )
            if (current.elem.equals(o)) {
                unbind(current);
                return true;
            }

        return false;
    }

    private T unbind(Node<T> node){
        T elem = node.elem;
        node.elem = null;

        Node<T> next = node.next;
        Node<T> prev = node.prev;

        size--;

        if( next == null ){
            last = prev;
        }else{
            next.prev = prev;
            node.next = null;
        }

        if( prev == null ){
            first = next;
        }else{
            prev.next = next;
            node.prev = null;
        }
        return elem;
    }


    @Override
    public boolean offer(T t) {
        return add(t);
    }

    @Override
    public T remove() {
        return removeFirst();
    }


    @Override
    public T poll() {
        return first == null ? null : removeFirst();
    }

    @Override
    public T element() {
        return getFirst();
    }

    @Override
    public T peek() {
        return first == null ? null : getFirst();
    }

    @Override
    public void push(T t) {
        addFirst(t);
    }

    @Override
    public T pop() {
        return removeFirst();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<T> descendingIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

}
